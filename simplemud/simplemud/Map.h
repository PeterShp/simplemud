#pragma once
using namespace std;

class Map
{
public:
	Map();
	~Map()
	{
	}
	int paintcolor[11] = {160,32,144,15,128,64,240,176,192,224,208};
	void border(int size) {
		for (int i = 0; i < size; i++) {
			cout << "__";
		}
		cout << "" << "\n";
	}
	vector <int> cells;
	int size;
	void load(string filename, int _size);
	int& operator()(int x, int y);
	void print(int xc, int yc);
};
