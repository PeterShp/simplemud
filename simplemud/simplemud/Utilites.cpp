#include "pch.h"

void gotoxy(int x, int y) {
	COORD pos = { x, y };
	HANDLE output = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleCursorPosition(output, pos);
}
void mapcolor(int x){
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), x);
}