#pragma once

struct Enemy {
	string Name;				//��� ����������
	int HP;						//���������� ����� ����� � ����������
	int Damage;					//����, ��������� ������ �� ����������
	int CritChance;				//���� ������������ ��������� �� ����, 100% = 100
	int DefenceChance[2][3];	//������� ������ ������
	int DamageChance[6];		//������� ������ �����
	int Loot;					//���������� ����������, ���������� ������� ����� ������ ��� �����������
	bool YouCanEscape;			//����������� ������� �� ����������
};

struct Weapon {
	string Name;				//�������� ������
	int Tier;					//������� ��������
	int Damage;					//���� �����
	int DamageType[2];			//������, �������� ������ � ���� ���������: 1. ��� ����� (����������, DoT, ����������), 2. ��� ��������� ����� (�������(0), �������(1))
	int CritChance;				//���� ������������ ��������� �� ����, 100% = 100
	int GemPerShot;				//����������� ��� ������������� ���������� ����������
	int Price;					//��������� � ��������
};

struct Armor {
	string Name;				//�������� �����
	int Tier;					//������� ��������
	float Multiplier;			//����������� ������
	int Price;					//��������� � ��������
};

struct Relic {
	string Name;				//�������� ��������
	int Effect;					//��� �������
	int EffectStrenght;			//���� ������� (��������, ���-�� ����������������� �� �� 1 ���)
	int Price;					//��������� � ��������
};

struct Potion {
	string Name;				//�������� ��������
	string sEffect;				//�������� �������
	bool Type;					//��� ����� (������������� �� ��� ��� �������������)
	int Effect;					//��� ������� (��������, �����������(0))
	int EffectStrenght;			//���� ������� (��������, ���-�� ����������������� �� �� 1 ���)
	int EffectTime;				//����� �������� �������
	int Price;					//��������� � ��������
};

class PlayerSystem {
private:
	int faction = 0;						//������� ���������, 0 - ��� �������, 1 - ���� (��������� �� "������ ����")
	bool haveShip = 0;						//����������� ������ �� ������ �������
	bool haveMountainItem = 0;				//����������� ������ �� �����
	short int armorEqp = 0;					//������� �����, ��������� � ���� id
	short int armor[3] = {0,0,0};			//����� � ���������, ��������� � ���� id
	short int relicEqp = 0;		//������� ��������, ��������� � ���� id
	short int relic[2] = {0,0};				//��������, ����������� � ���������, ��������� � ���� id
	short int weapon[5] = {0,0,0,0,0};		//������ � ���������, ��������� � ���� id
	short int potion[6] = {0,0,0,0,0,0};	//����� � ���������, ��������� � ���� id
	short int gem = 10000;						//������� ���-�� ����������
	short int nowHP = 10;					//������� ���������� ����� ��������
	short int maxHP = 100;					//����������� ��������� ���-�� ��������
	short int addMaxHP[2] = { 0,0 };					//���������� ������������� ���-�� �� � ������� �����
	short int regenHP[3] = {0,0,0};			//������, �������� ������ � ����������� ��������: 1. ������������ �����������, 2. ������� �����������, 3. ������������ ������� ����������� (�������� ��������� � �������� ������������ �����������)
	short int coordinate[2] = { 0,0 };		//���������� �� �����.
public:
	vector <Weapon> allWeapons;				//������ ����� ������, ����������� �����
	vector <Armor> allArmors;				//������ ���� �����, ����������� �����
	vector <Relic> allRelics;				//������ ���� ��������, ����������� �����
	vector <Potion> allPotions;				//������ ���� �����, ����������� �����
	vector <vector<Enemy>> EnemyList;
	//�������� ��������������� ��������
	void setFaction(int f) {	
		faction = f;
	}
	
	void rebalance() {
		// ������� ��������
	}

	//�������� � �������
	short int getGem() {
		return gem;
	}

	bool addGem(int x) {
		gem += x;
		return 1;
	}

	bool removeGem(int x) {
		if (gem >= x) {
			gem -= x;
			return 1;
		}
		else {
			return 0;
		}
	}
	// �������� � ��
	// 0 - ������, 1 - ������������
	short int getHP(bool x) { 
		if (x) {
			return maxHP+addMaxHP[0];
		}
		else {
			return nowHP;
		}
	}

	void removeHP(int x) {
		if (x >= getHP(0)) {
			nowHP = 0;
		}
		else {
			nowHP -= ceil(x*(1-allArmors[armorEqp].Multiplier));
		}
	}
	
	bool initAddMaxHP(int strength, int time) {
		if (addMaxHP[0]>0) {
			return 0;
		}
		else {
			addMaxHP[0] = strength;
			addMaxHP[1] = time;
			return 1;
		}
	}

	void controlAddMaxHP() {
		if (addMaxHP[0] > 0&&addMaxHP[1]<=1) {
			addMaxHP[0] = 0;
			addMaxHP[1] = 0;
		}
		else {
			addMaxHP[1]--;
		}
	}

	// ����������� ��
	bool regHP() {
		nowHP += regenHP[0];
		if (regenHP[2] != 0) {
			nowHP += regenHP[1];
			regenHP[2]--;
			if (regenHP[2] == 0) {
				regenHP[1] = 0;
			}
			}
		if (nowHP > getHP(0)) {
			nowHP = getHP(0);
		}
		return 1;
	}
	bool addHP(int x) {
		nowHP += x;
		if (nowHP > getHP(0)) {
			nowHP = getHP(0);
		}
		return 1;
	}

	// ��������� ����� ����������� ��
	// HP - ���-�� �������������� �� �� 1 ���
	// time - ���-�� ��������� �����������
	bool initRegHP(int HP, int time) {
		if (regenHP[2] == 0) {
			regenHP[1] = HP;
			regenHP[2] = time;
			return 1;
		}
		else {
			return 0;
		}
	}

	// �������� � ������
	Armor getArmor(int x) {
		return allArmors[armor[x]];
	}

	Armor nowArmor() {
		return allArmors[armorEqp];
	}

	bool addArmor(int x) {
		for (int i = 0; i < allArmors[armor[x]].Tier; i++) {
			if (armor[i] == 0) {
				armor[i] = x;
				return 1;
			}
		}
		return 0;
	}

	bool removeArmor(int x) {
		if (armor[x] < 1) {
			return 0;
		}
		else {
			armor[x] = 0;
			return 1;
		}
	}

	int nowTier() {
		return allArmors[armorEqp].Tier;
	}

	Relic nowRelic() {
		return allRelics[relicEqp];
	}

	bool equipArmor(int x) {
		if (armor[x] > 0) {
			if (allArmors[armor[x]].Tier == allArmors[armorEqp].Tier) { // ���� ��� ����������
				swap(armor[x], armorEqp);
			}
			else if (allArmors[armor[x]].Tier > allArmors[armorEqp].Tier) { // ���� ��� ����
				swap(armor[x], armorEqp);
				// ���������� ����� ��� �����/��������
				for (int i = 0; i < allArmors[armorEqp].Tier; i++) {
					if (armor[i] == 0) {
						armor[i] = 0;
					}
				}
				// ���������� ����� ��� �����
				for (int i = 0; i < floor((1 + allArmors[armorEqp].Tier) * 1.5); i++) {
					if (potion[i] == 0) {
						potion[i] = 0;
					}
				}
				// ���������� ����� ��� ������
				for (int i = 0; i < ceil(allArmors[armorEqp].Tier * 1.5); i++) {
					if (weapon[i] == 0) {
						weapon[i] = 0;
					}
				}
			}
			else if (allArmors[armor[x]].Tier < allArmors[armorEqp].Tier) { // ���� ��� ����
				bool responce = 1;
				if (responce) {
					swap(armor[x], armorEqp);
					//�������� ����� � ������ ������� ��������
					for (int i = 0; i < 3; i++) {
						if (i > allArmors[armorEqp].Tier) {
							armor[i] = 0;
						}
					}
					//�������� ������ �����
					for (int i = 0, j = floor((1 + allArmors[armorEqp].Tier) * 1.5); i < 6; i++) {
						if (i > j) {
							potion[i] = 0;
						}
					}
					//�������� ������� ������
					for (int i = 0, j = ceil(allArmors[armorEqp].Tier * 1.5); i < 5; i++) {
						if (i > j) {
							weapon[i] = 0;
						}
					}
					return 1;
				}
				else {
					return 0;
				}
			}
		}
		else {
			return 0;
		}
	}

	void equipRelic(int x, int y) {
		swap(relic[x], relicEqp);
	}

	// �������� � �������
	Weapon getWeapon(int x) {
		return allWeapons[weapon[x]];
	}

	bool addWeapon(int x) {
		for (int i = 0, j = ceil(allArmors[armorEqp].Tier * 1.5); i < j; i++) {
			if (weapon[i] == 0) {
				if (removeGem(allWeapons[x].Price)) {
					weapon[i] = x;
					return 1;

				}

			}
		}
		return 0;
	}

	bool removeWeapon(int x) {
			weapon[x] = 0;
			return 1;
	}

	bool useWeapon(int x) {
		if (gem >= allWeapons[weapon[x]].GemPerShot) {
			gem -= allWeapons[weapon[x]].GemPerShot;
			return 1;
		}
		else {
			return 0;
		}
	}

	// �������� � ����������
	Relic getRelic(int x) {
		return allRelics[relic[x]];
	}

	bool addRelic(int x) {
		for (int i = 0, j = floor((1 + allArmors[armorEqp].Tier) * 1.5); i < j; i++) {
			if (relic[i] == 0) {
				relic[i] = x;
				return 1;
			}
		}
		return 0;
	}

	bool removeRelic(int x) {
		if (relic[x] < 1) {
			return 0;
		}
		else {
			relic[x] = 0;
			return 1;
		}
	}

	// �������� � �������

	Potion getPotion(int x) {
		return allPotions[potion[x]];
	}

	bool addPotion(int x) {
		for (int i = 0, j = floor((1 + allArmors[armorEqp].Tier) * 1.5); i < j; i++) {
			if (potion[i] == 0) {
				potion[i] = x;
				return 1;
			}
		}
		return 0;
	}

	bool removePotion(int x) {
		if (weapon[x] < 1) {
			return 0;
		}
		else {
			weapon[x] = 0;
			return 1;
		}
	}

	bool usePotion(int x) {
		for (int i = 0, j = floor((1 + allArmors[armorEqp].Tier) * 1.5); i < j; i++) {
			if (potion[i] == x) {
				potion[i] = 0;
				return 1;
			}
		}
		return 0;
	}

	//�������� � ������������

	void move(int x, int y) {
		coordinate[0] += x;
		coordinate[1] += y;
		if (coordinate[0] == 16 || coordinate[0] == -16) {
			coordinate[0] = 0;
		}
		if (coordinate[1] == 16 || coordinate[1] == -16) {
			coordinate[1] = 0;
		}
	}

	int getCoordinateX() {
		return coordinate[0];
	}

	int getCoordinateY() {
		return coordinate[1];
	}

	//NPC

	Enemy setEnemy(int id) {
		return EnemyList[id][rand() % EnemyList[id].size()];
	}

};