﻿#include "pch.h"

Map::Map()
{
}

struct MapCells {
	int type[3] = {3,9,10};
	bool allow[3] = { false, false, false };
	/*bool check(int x) {
		//if ()
	}*/
};

void Map::load(string filename, int _size) {
	size = _size;
	ifstream maptxt(filename);
	for (int i = 0; i < size; i++) {
		string temp;
		getline(maptxt, temp);
		istringstream tokenStream(temp);
		for (int j = 0; j < size; j++) {
			getline(tokenStream, temp, ' ');
			cells.push_back(atoi(temp.c_str()));
		}
	}
}

void Map::print(int xc, int yc) {
	gotoxy(0, 0);
	for (int dy = 0; dy < size; dy++) {
		for (int dx = 0; dx < size; dx++) {
			if (xc - size / 2 + dx == xc && yc - size / 2 + dy == yc) {
				mapcolor(224);
				cout << "@@";
			}
			else {
				mapcolor(paintcolor[(*this)(xc - size / 2 + dx, yc - size / 2 + dy)]);
				cout << "  ";
			}
		}
		mapcolor(8);
		cout << " |" << "\n";
	}
	border(size);
}

int& Map::operator()(int x, int y) {
	x %= size;
	if (x < 0) x += size;
	y %= size;
	if (y < 0) y += size;
	return cells[x + y * size];
}
