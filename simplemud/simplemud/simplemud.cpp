﻿#include "pch.h"

PlayerSystem preLoad(PlayerSystem);

vector <vector <Enemy>> EnemyList;
PlayerSystem battle(PlayerSystem Player, int id, int dfc) {
	system("cls");
	Enemy enemy; enemy.CritChance = 0; enemy.Damage = 10; enemy.HP = floor(100 * ((100 + dfc) / 100)); enemy.Name = "RUSSIA"; enemy.YouCanEscape = 0;
	// допустим тут подстаивается dfc
	int action[3] = { 0,0,0 };
	int update = 1;
	while (action[0] == 0) {
		while (update == 1) {
			cout << "Hey, you entered in the battle! Your enemy: " << enemy.Name << ", and he have " << enemy.HP << " HP." << endl << "You want to leave this battle?" << endl;
			if (action[1] == 0) {
				cout << " >> ";
			}
			cout << "1. No" << endl;
			if (action[1] == 1) {
				cout << " >> ";
			}
			cout << "2. Yes" << endl;
			update = 0;
			while (update == 0) {
				if (GetKeyState(VK_DOWN) & 0x8000) {
					action[1]++;
					update = 1;
				}
				else if (GetKeyState(VK_UP) & 0x8000) {
					action[1]--;
					update = 1;
				}
				else if (GetKeyState(VK_HOME) & 0x8000) {
					if (action[1] == 1) {
						if (enemy.YouCanEscape == 1) {
							return Player;
						}
						else {
							cout << enemy.Name << " did not let you go, hitting you." << endl << "You lose " << enemy.Damage << " HP." << endl;
							Player.removeHP(enemy.Damage);
							action[1] = 0;
							system("pause");
						}
					}
					if (action[1] == 0) {
						action[0] = 1;
						break;
					}
				}
				if (update == 1) {
					if (action[1] == -1) {
						action[1] = 1;
					}
					else if (action[1] == 2) {
						action[1] = 0;
					}
					Sleep(100);
				}
			}
			system("cls");
		}
	}
	action[1] = 0; action[2] = 0;
	int lastDamage[2] = { 0,0 };
	bool criticals;
	bool endTurn = 0;
	int Defence[2][2];		//1 - моб, 0 - игрок
	int Damage[3]; 
	int Poison[2][2];
	int Potion;
	int addCrit[2];
	bool skip = false;
	for (int i = 0; i > 2; i++) {
		for (int j = 0; j > 2; j++) {
			Defence[j][i] = 0;
		}
	}
	update = 1;
	action[0] = 0;
	system("pause");
	while (Player.getHP(0) > 0 && enemy.HP > 0) {
		while (update == 0) {
			system("pause");
			if (GetKeyState(VK_SPACE) & 0x8000) {
				action[2] = 1;
				update = 1;
			}
			else if (GetKeyState(VK_UP) & 0x8000) {
				action[1]--;
				update = 1;
			}
			else if (GetKeyState(VK_DOWN) & 0x8000) {
				action[1]++;
				update = 1;
			}
			else {
				update = 1;
			}
		}
		if (action[2] == 1) {
			switch (action[0]) {
			case 0:
				if (action[1] == 0) {
					action[0] = 1;
				}
				else {
					action[0] = 4;
				}
				break;
			case 1:
				Defence[0][0] = action[1];
				action[0] = 2;
				break;
			case 2:
				Defence[0][1] = action[1];
				action[0] = 3;
				break;
			case 3:
				Damage[0] = action[1];
				endTurn = 1;
				break;
			case 4:
				if (Player.getPotion(action[1]).Type) {
					switch (Player.getPotion(action[1]).Effect) {
					case 1:
						Player.addHP(Player.getPotion(action[1]).EffectStrenght);
						Player.removePotion(action[1]);
						break;
					case 2:
						Player.initRegHP(Player.getPotion(action[1]).EffectStrenght, Player.getPotion(action[1]).EffectTime);
						Player.removePotion(action[1]);
						break;
					case 3:
						if (Player.initAddMaxHP(Player.getPotion(action[1]).EffectStrenght, Player.getPotion(action[1]).EffectTime)) {
							Player.removePotion(action[1]);
						}
						break;
					case 4:
						if (addCrit[0] > 0) {

						}
						else {
							addCrit[0] = Player.getPotion(action[1]).EffectStrenght;
							addCrit[1] = Player.getPotion(action[1]).EffectTime;
							Player.removePotion(action[1]);
						}
						break;
					}
					endTurn = 1;
				}
				else {
					// Эффекты зелий:
					// 1 - моментальный хил
					// 2 - регенерация
					// 3 - увеличение макс. ХП
					// 4 - увеличение критического шанса
					switch (Player.getPotion(action[1]).Effect) {
					case 1:
						Player.addHP(Player.getPotion(action[1]).EffectStrenght);
						Player.removePotion(action[1]);
						break;
					case 2:
						Player.initRegHP(Player.getPotion(action[1]).EffectStrenght, Player.getPotion(action[1]).EffectTime);
						Player.removePotion(action[1]);
						break;
					case 3:
						if (Player.initAddMaxHP(Player.getPotion(action[1]).EffectStrenght, Player.getPotion(action[1]).EffectTime)) {
							Player.removePotion(action[1]);
						}
						break;
					case 4:
						if (addCrit[0] > 0) {
							
						}
						else {
							addCrit[0] = Player.getPotion(action[1]).EffectStrenght;
							addCrit[1] = Player.getPotion(action[1]).EffectTime;
							Player.removePotion(action[1]);
						}
						break;
					}

				}
				action[0] = 0;
			}
			break;
		}
		action[2] = 0;
	
		if (update == 1) {
			system("cls");
			update = 0;
			cout << "You:" << endl;
			cout << "HP: " << Player.getHP(0) << endl;
			cout << "Gem: " << Player.getGem() << endl;
			cout << "Last damage: " << lastDamage[0] << endl; // update заменить на переменную
			cout << enemy.Name << ":" << endl;
			cout << "HP: " << enemy.HP << endl;
			cout << "Last damage: " << lastDamage[1] << endl << endl << endl;

			switch (action[0]) {
			case 0:
				if (action[1] > 1) {
					action[1] = 0;
				}
				else if (action[1] < 0) {
					action[1] = 1;
				}
				if (action[1] == 0) {
					cout << ">>\t";
				}
				cout << "1. Battle" << endl;
				if (action[1] == 1) {
					cout << ">>\t";
				}
				cout << "2. Potions" << endl;
				break;
			case 1:
				if (action[1] > 2) {
					action[1] = 0;
				}
				else if (action[1] < 0) {
					action[1] = 2;
				}
				cout << "Defence side: Short distance" << endl;
				if (action[1] == 0) {
					cout << ">>\t";
				}
				cout << "1. Physical damage" << endl;
				if (action[1] == 1) {
					cout << ">>\t";
				}
				cout << "2. DoT damage" << endl;
				if (action[1] == 2) {
					cout << ">>\t";
				}
				cout << "3. Magic damage" << endl;
				break;
			case 2:
				if (action[1] > 2) {
					action[1] = 0;
				}
				else if (action[1] < 0) {
					action[1] = 2;
				}
				cout << "Defence side: Far distance" << endl;
				if (action[1] == 0) {
					cout << ">>\t";
				}
				cout << "1. Physical damage" << endl;
				if (action[1] == 1) {
					cout << ">>\t";
				}
				cout << "2. DoT damage" << endl;
				if (action[1] == 2) {
					cout << ">>\t";
				}
				cout << "3. Magic damage" << endl;
				break;
			case 3:
				if (action[1] > ceil(Player.nowTier() * 1.5)) {
					action[1] = 0;
				}
				else if (action[1] < 0) {
					action[1] = ceil(Player.nowTier() * 1.5);
				}
				cout << "Choose weapon" << endl;
				for (int i = 0; i < ceil(Player.nowTier() * 1.5); i++) {
					if (action[1] == i) {
						cout << ">>\t";
					}
					cout << Player.getWeapon(i).Name << "\tDamage: " << Player.getWeapon(i).Damage << "\tPrice: " << Player.getWeapon(i).Price << endl;
				}
				break;
			case 4:
				if (action[1] > ceil((1 + Player.nowTier()) * 1.5)) {
					action[1] = 0;
				}
				else if (action[1] < 0) {
					action[1] = ceil((1 + Player.nowTier()) * 1.5);
				}
				cout << "Choose potion" << endl;
				for (int i = 0; i < ceil((1 + Player.nowTier()) * 1.5); i++) {
					cout << Player.getPotion(i).Name << "\tEffect: " << Player.getPotion(i).sEffect << endl;
				}
				break;
			}
			update = 0;
		}
		
		if (endTurn) {
			system("cls");
			if (Poison[0][1] > 0) {
				Poison[0][1]--;
				enemy.HP -= Poison[0][0];
				if (Poison[0][1] == 0) {
					Poison[0][0] = 0;
				}
			}
			if (Poison[1][1] > 0) {
				Poison[1][1]--;
				Player.removeHP(Poison[1][0]);
				if (Poison[1][1] == 0) {
					Poison[1][0] = 0;
				}
			}
			for (bool comp0 = 1, comp1 = 0, comp2 = 0; comp0;) {
				while (!comp1) {
					if (!comp1 && rand() % 100 > enemy.DefenceChance[0][0]) {
						Defence[1][0] = 1;
						comp1 = 1;
					}
					else if (!comp1 && rand() % 100 > enemy.DefenceChance[0][1]) {
						Defence[1][0] = 2;
						comp1 = 1;
					}
					else if (!comp1 && rand() % 100 > enemy.DefenceChance[0][2]) {
						Defence[1][0] = 3;
						comp1 = 1;
					}
				}
				while (!comp2) {
					if (!comp2 && rand() % 100 > enemy.DefenceChance[1][0]) {
						Defence[1][1] = 1;
						comp2 = 1;
					}
					else if (!comp2 && rand() % 100 > enemy.DefenceChance[1][1]) {
						Defence[1][1] = 2;
						comp2 = 1;
					}
					else if (!comp2 && rand() % 100 > enemy.DefenceChance[1][1]) {
						Defence[1][1] = 3;
						comp2 = 1;
					}
				}
				if (comp1 && comp2) {
					comp0 = 0;
				}
			}
			for (bool comp = 1; comp;) {
				if (comp && rand() % 100 > enemy.DamageChance[0]) {
					Damage[1] = 0;
					Damage[2] = 1;
				}
				else if (comp && rand() % 100 > enemy.DamageChance[1]) {
					Damage[1] = 0;
					Damage[2] = 2;
				}
				else if (comp && rand() % 100 > enemy.DamageChance[2]) {
					Damage[1] = 0;
					Damage[2] = 3;
				}
				else if (comp && rand() % 100 > enemy.DamageChance[3]) {
					Damage[1] = 1;
					Damage[2] = 1;
				}
				else if (comp && rand() % 100 > enemy.DamageChance[4]) {
					Damage[1] = 1;
					Damage[2] = 2;
				}
				else if (comp && rand() % 100 > enemy.DamageChance[5]) {
					Damage[1] = 1;
					Damage[2] = 3;
				}
			}
			if (rand() % 100 > enemy.CritChance) {
				if (Defence[0][Damage[1]] == Damage[2]) {
					cout << "You foresaw the action of the enemy, but this did not save you from taking damage." << endl;
					lastDamage[0] = enemy.Damage;
					Player.removeHP(floor(enemy.Damage));
				}
				else {
					cout << "Oh, maybe, it`s a painfull." << endl << "You lose " << ceil(floor(enemy.Damage * 1.5)*Player.nowArmor().Multiplier) << " HP" << endl;
					lastDamage[0] = floor(enemy.Damage * 1.5);
					Player.removeHP(floor(enemy.Damage * 1.5));
				}
				if (Player.getWeapon(Damage[0]).DamageType[0] == 2) {
					if (Poison[0][1] != 0 && Poison[0][0] == Player.getWeapon(Damage[0]).Damage) {
						Poison[0][0] = Player.getWeapon(Damage[0]).Damage;
						Poison[0][1] += 3;
					}
					else {
						Poison[0][0] = Player.getWeapon(Damage[0]).Damage;
						Poison[0][1] = 3;
					}
				}
			}
			else {
				if (Defence[0][Damage[1]] == Damage[2]) {
					cout << "You foresaw the action of the enemy, thanks to which you are completely protected from the blow." << endl;
					lastDamage[0] = 0;
				}
				else {
					cout << "You lose " << enemy.Damage << " HP" << endl;
					lastDamage[0] = enemy.Damage;
					Player.removeHP(floor(enemy.Damage));
				}
			}
			//Нанесение урона ИГРОКОМ
			if (!skip) {
				if (Player.useWeapon(Damage[1])) {
					if (rand() % 100 > Player.getWeapon(Damage[0]).CritChance + addCrit[0]) {
						if (Defence[1][Player.getWeapon(Damage[0]).DamageType[1]] == Player.getWeapon(Damage[0]).DamageType[0]) {
							cout << "You break a enemy`s defence side! Enemy lose " << Player.getWeapon(Damage[0]).Damage << " HP" << endl;
							enemy.HP -= Player.getWeapon(Damage[0]).Damage;
							if (Player.getWeapon(Damage[0]).DamageType[0] == 2) {
								if (Poison[0][1] != 0 && Poison[0][0] == Player.getWeapon(Damage[0]).Damage) {
									Poison[0][0] = Player.getWeapon(Damage[0]).Damage;
									Poison[0][1] += 3;
								}
								else {
									Poison[0][0] = Player.getWeapon(Damage[0]).Damage;
									Poison[0][1] = 3;
								}
							}
						}
						else {
							cout << "You deal a critical hit! Enemy lose " << Player.getWeapon(Damage[0]).Damage << " HP" << endl;
							enemy.HP -= floor(Player.getWeapon(Damage[0]).Damage * 1.5);
							lastDamage[1] = floor(Player.getWeapon(Damage[0]).Damage * 1.5);
							if (Player.getWeapon(Damage[0]).DamageType[0] == 2) {
								if (Poison[0][1] != 0 && Poison[0][0] == Player.getWeapon(Damage[0]).Damage) {
									Poison[0][0] = Player.getWeapon(Damage[0]).Damage;
									Poison[0][1] += 3;
								}
								else {
									Poison[0][0] = Player.getWeapon(Damage[0]).Damage;
									Poison[0][1] = 3;
								}
							}
						}
					}
					else {
						if (Defence[1][Player.getWeapon(Damage[0]).DamageType[1]] == Player.getWeapon(Damage[0]).DamageType[0]) {
							cout << "The enemy successfully defended against your blow." << endl;
							lastDamage[1] = 0;
						}
						else {
							cout << "You hit your enemy. He lose " << Player.getWeapon(Damage[0]).Damage << " HP" << endl;
							enemy.HP -= Player.getWeapon(Damage[0]).Damage;
							lastDamage[1] = Player.getWeapon(Damage[0]).Damage;
							if (Player.getWeapon(Damage[0]).DamageType[0] == 2) {
								if (Poison[0][1] != 0 && Poison[0][0] == Player.getWeapon(Damage[0]).Damage) {
									Poison[0][0] = Player.getWeapon(Damage[0]).Damage;
									Poison[0][1] += 3;
								}
								else {
									Poison[0][0] = Player.getWeapon(Damage[0]).Damage;
									Poison[0][1] = 3;
								}
							}
						}
					}
				}
				else {
					cout << "You don't shoot in this turn, because you don't have some crystals." << endl;
				}
			}
			Player.controlAddMaxHP();
			Player.regHP();
			if (addCrit[1] > 0) {
				addCrit[1]--;
			}
			else {
				addCrit[0] = 0;
				addCrit[1] = 0;
			}
			endTurn = 0;
		}
	}
	if (Player.getHP(0) <= 0) {
		cout << "You die." << endl;
		return Player;
	}
	else {
		cout << "You kill " << enemy.Name << "!";
		Player.addGem(enemy.Loot);
		cout << "You got " << enemy.Loot << " crystals.";
	}
	return Player;
}

bool greeting() {
	string disclaimer[10];
	disclaimer[0] = { "                      WARNING!" };
	disclaimer[1] = { "\n" };
	disclaimer[2] = { "      In-game control is carried out using the" };
	disclaimer[3] = { "    arrow buttons, as well as using the space bar." };
	disclaimer[4] = { "	 We accept no responsibility for the use" };
	disclaimer[5] = { "           of this entertainment software." };
	disclaimer[6] = { "             The game does not have an" };
	disclaimer[7] = { "               ultimate goal, but it" };
	disclaimer[8] = { "                may end if you die." };
	disclaimer[9] = { "                 Have a nice game!" };
	cout << endl << endl << endl;
	for (int i = 0; i < 10; i++) {
		cout << "\t\t\t";
		for (int j = 0; j < disclaimer[i].length(); j++) {

			cout << disclaimer[i][j];
			Sleep(5);
		}
		cout << endl;
	}
	cout << endl << endl << endl << "\t\t\t\t\t  (wait 5 second)";
	Sleep(5000);
	system("cls");
	string logo[17];
	logo[0] = { "      _                 _                          _ " };
	logo[1] = { "     (_)               | |                        | |" };
	logo[2] = { "  ___ _ _ __ ___  _ __ | | ___ _ __ ___  _   _  __| |" };
	logo[3] = { " / __| | '_ ` _ \\| '_ \\| |/ _ \\ '_ ` _ \\| | | |/ _` |" };
	logo[4] = { " \\__ \\ | | | | | | |_) | |  __/ | | | | | |_| | (_| |" };
	logo[5] = { " |___/_|_| |_| |_| .__/|_|\\___|_| |_| |_|\\__,_|\\__,_|" };
	logo[6] = { "                 | |                                 " };
	logo[7] = { "                 |_|                                 " };
	switch (rand() % 10) {
	case 0:
		logo[8] = { "\n\t\t\t\tv.preAlpha 0.001" };
		break;
	case 1:
		logo[8] = { "\n\t\t\t\tHave you already bought a new DLC?" };
		break;
	case 2:
		logo[8] = { "\n\t\t\t\t(not)Inside the patch of the first day!" };
		break;
	case 3:
		logo[8] = { "\n\t\t\t\tThe new BattlePass season is coming." };
		break;
	case 4:
		logo[8] = { "\n\t\t\t\tNew skins are on sale now!" };
		break;
	case 5:
		logo[8] = { "\n\t\t\t\tGood time for donation!" };
		break;
	case 6:
		logo[8] = { "\n\t\t\t\tJust today - 99% off new pixels!" };
		break;
	case 7:
		logo[8] = { "\n\t\t\t\tAll mobs are 100% friendly! (but it is not exactly)" };
		break;
	case 8:
		logo[8] = { "\n\t\t\t\tDonat affects the gameplay!" };
		break;
	case 9:
		logo[8] = { "\n\t\t\t\tUnrelated to Open Source" };
		break;
	case 10:
		logo[8] = { "\n\t\t\t\tHello Hackers" };
			break;
	}
	for (int i = 0; i < 9; i++) {
		cout << "\t\t\t";
		for (int j = 0; j < logo[i].length(); j++) {
			cout << logo[i][j];
			Sleep(2.5);
		}
		cout << endl;
	}
	cout << endl << endl << endl << endl << endl << endl << endl << endl << endl << endl << endl << endl << endl << endl << endl << endl << endl << endl << "\t\t\t";
	system("pause");
	system("cls");
	bool run = 0;
	bool update = 1;
	int action = 0;
	while (!run) {
		while (!update) {
			system("pause");
			if (GetKeyState(VK_SPACE) & 0x8000) {
				update = 1;
				run = 1;
			}
			else if (GetKeyState(VK_UP) & 0x8000) {
				action--;
				update = 1;
			}
			else if (GetKeyState(VK_DOWN) & 0x8000) {
				action++;
				update = 1;
			}
			else {
				update = 1;
			}
		}
		if (update) {
			if (action > 1) {
				action = 0;
			}
			else if (action < 0) {
				action = 1;
			}
			system("cls");
			if (action == 0) {
				cout << "\t>> ";
			}
			cout << "Start game" << endl;
			if (action == 1) {
				cout << "\t>> ";
			}
			cout << "Exit game" << endl;
			update = 0;
		}
		if(run) {
			system("cls");
			return action;
		}
	}
}



void game() {
	PlayerSystem Player;
	Player = preLoad(Player);
	Map map;
	map.load("Map.txt", 16);
	// Переменные
	int action[4] = { 0,0,0,0 };
	int selectedTier = 0;
	int slot;
	bool game = 1;
	bool update[2] = { 1,1 }; // [0] - обычный update, [1] - вывод карты
	//Игра
	while (game) {
		if (Player.getHP(0) <= 0) {
			system("cls");
			cout << "You lose, the game will now close." << endl << " If you want to start again, just start the game again." << endl;
			system("pause");
			return;
		}
		while (!update[0]) {
			system("pause");
			if (GetKeyState(VK_SPACE) & 0x8000) {
				action[3] = 1;
			}
			else if (GetKeyState(VK_UP) & 0x8000) {
				action[1]--;
			}
			else if (GetKeyState(VK_DOWN) & 0x8000) {
				action[1]++;
			}
			else if (GetKeyState(VK_RIGHT) & 0x8000) {
				action[2] = 3;
			}
			else if (GetKeyState(VK_LEFT) & 0x8000) {
				action[2] = 4;
			}
			update[0] = 1;
		}
		if (action[2] > 2) {
			switch (action[2]) {
			case 3:
				switch (action[0]) {
				case 5:
					break;
				case 6:
					break;
				case 7:
					break;
				case 8:
					break;
				}
			case 4:
				switch (action[0]) {
				case 5:
					Player.removeWeapon(action[1]-1);
					break;
				case 6:
					break;
				case 7:
					break;
				case 8:
					break;
				}
			}
			action[2] = 0;
		}
		if (action[3] == 1) {
			switch (action[0]) {
			case 0:
				switch (action[1]) {
				case 0:
					action[0] = 1;
					break;
				case 1:
					action[0] = 4;
					break;
				case 2:
					action[0] = 9;
					break;
				}
				break;
			case 1:
				switch (action[1]) {
				case 0:
					action[0] = 2;
					break;
				case 1:
					action[0] = 3;
					break;
				case 2:
					action[0] = 0;
					break;
				}
				break;
			case 2:
				switch (action[1]) {
					//РЕЗЕРВ
				}
				break;
			case 3:
				switch (action[1]) {
					//РЕЗЕРВ
				}
				break;
			case 4:
				switch (action[1]) {
				case 0:
					action[0] = 0;
					break;
				case 1:
					action[0] = 5;
					break;
				case 2:
					action[0] = 6;
					break;
				case 3:
					action[0] = 7;
					break;
				case 4:
					action[0] = 8;
					break;
				}
				break;
			case 5:
				switch (action[1]) {
				case 0:
					action[0] = 0;
					break;
				}
				break;
			case 6:
				switch (action[1]) {
				case 0:
					action[0] = 0;
					break;
				case (1 || 2 || 3):
					Player.equipArmor(action[0] - 1);
					break;
				}
				break;
			case 7:
				switch (action[1]) {
				case 0:
					action[0] = 0;
					break;
				}
				break;
			case 8:
				switch (action[1]) {
				case 0:
					action[0] = 0;
					break;
				}
				break;
			case 9:
				switch (action[1]) {
				case 0:
					action[0] = 0;
					break;
				case 1:
					action[0] = 10;
					selectedTier = action[1];
					break;
				case 3:
					action[0] = 10;
					selectedTier = action[1];
					break;
				case 2:
					action[0] = 10;
					selectedTier = action[1];
					break;
				}
				break;
			case 10:
				switch (action[1]) {
				case 0:
					action[0] = 0;
					break;
				case 1:
					action[0] = 10 + action[1];
					break;
				case 2:
					action[0] = 10 + action[1];
					break;
				case 3:
					action[0] = 10 + action[1];
					break;
				case 4:
					action[0] = 10 + action[1];
					break;
				}
				break;
			case 11:
				switch (action[1]) {
				case 0:
					action[0] = 0;
					break;
				default:
					Player.addWeapon(action[1] + (6 * (selectedTier - 1)));
					break;
					
				}
				// action[1] + (6 * (selectedTier - 1))
				break;
			case 12:
				switch (action[1]) {
					action[0] = 0;
					break;
				default:
					Player.addArmor(action[1] + (1 * (selectedTier - 1)));
					break;
				}
				break;
			case 13:
				switch (action[1]) {
					action[0] = 0;
					break;
				default:
					Player.addPotion(action[1]);
					break;
				}
				break;
			case 14:
				switch (action[1]) {
					action[0] = 0;
					break;
				default:
					Player.addRelic(action[1]);
					break;
				}
				break;
			}
			action[1] = 0;
		}
		
		if (update[0]) {
			system("cls");
			if (update[1]) {
				//ТУТ ВЫВОД КАРТЫ И СТАТИСТИКИ
			}
			switch (action[0]) {
			case 0:
				// Основное меню
				if (action[2] > 0 && action[2] < 3) {
					if (action[2] == 1) {
						action[1]--;
					}
					else {
						action[1]++;
					}
				}
				if(action)
				if (true) {
					if (action[1] > 2) {
						action[1] = 0;
					}
					else if (action[1] < 0) {
						action[1] = 2;
					}
				}
				else {
					if (action[1] > 1) {
						action[1] = 0;
					}
					else if (action[1] < 0) {
						action[1] = 1;
					}
				}
				if (action[1] == 0) {
					cout << "\t>> ";
				}
				cout << "Map" << endl;
				if (action[1] == 1) {
					cout << "\t>> ";
				}
				cout << "Inventory" << endl;
				if (true) { // false заменить на проверку нахождения в городе!
					if (action[1] == 2) {
						cout << "\t>> ";
					}
					cout << "Store" << endl;
				}
				break;
			case 1:
				// Карта: Подменю
				if (action[2] > 0 && action[2] < 3) {
					if (action[2] == 1) {
						action[1]--;
					}
					else {
						action[1]++;
					}
				}
				if (action[1] > 2) {
					action[1] = 0;
				}
				else if (action[1] < 0) {
					action[1] = 2;
				}
				if (action[1] == 0) {
					cout << "\t>> ";
				}
				cout << "Explore the area" << endl;
				if (action[1] == 1) {
					cout << "\t>> ";
				}
				cout << "Keep going" << endl;
				if (action[1] == 2) {
					cout << "\t>> ";
				}
				cout << "Back" << endl;
				break;
			case 2:
				// Карта: Осмотр территории
				// вызов боя: Player = battle(Player,(id локации(для генерации мобов)),());
				break;
			case 3:
				// Карта: Передвижение между клетками
				break;
			case 4:
				// Инвентарь: Подменю
				if (action[2] > 0 && action[2] < 3) {
					if (action[2] == 1) {
						action[1]--;
					}
					else {
						action[1]++;
					}
				}
				if (action[1] > 4) {
					action[1] = 0;
				}
				else if (action[1] < 0) {
					action[1] = 4;
				}
				if (action[1] == 0) {
					cout << "\t>> ";
				}
				cout << "Back" << endl;
				if (action[1] == 1) {
					cout << "\t>> ";
				}
				cout << "Weapon" << endl;
				if (action[1] == 2) {
					cout << "\t>> ";
				}
				cout << "Armor" << endl;
				if (action[1] == 3) {
					cout << "\t>> ";
				}
				cout << "Potion" << endl;
				if (action[1] == 4) {
					cout << "\t>> ";
				}
				cout << "Relics" << endl;
				break;
			case 5:
				// Инвентарь: Оружие
				if (action[2] > 0 && action[2] < 3) {
					if (action[2] == 1) {
						action[1]--;
					}
					else {
						action[1]++;
					}
				}
				if (action[1] > ceil(Player.nowTier() * 1.5)) {
					action[1] = 0;
				}
				else if (action[1] < 0) {
					action[1] = ceil(Player.nowTier() * 1.5);
				}
				if (action[1] == 0) {
					cout << "\t>> ";
				}
				cout << "Exit" << endl;
				for (int i = 0; i < ceil(Player.nowTier() * 1.5); i++) {
					if (action[1] == i+1) {
						cout << "\t>> ";
					}
					cout << i + 1 << ". " << Player.getWeapon(i).Name << " / Tier: " << Player.getWeapon(i).Tier << " / Price per use: " << Player.getWeapon(i).GemPerShot << endl;
				}
				break;
			case 6:
				// Инвентарь: Броня
				if (action[2] > 0 && action[2] < 3) {
					if (action[2] == 1) {
						action[1]--;
					}
					else {
						action[1]++;
					}
				}
				if (action[1] > Player.nowTier()) {
					action[1] = 0;
				}
				else if (action[1] < 0) {
					action[1] = Player.nowTier();
				}
				cout << "EQP: " << Player.nowArmor().Name << " / Tier: " << Player.nowArmor().Tier << " Defence: " << Player.nowArmor().Multiplier << endl << endl;
				if (action[1] == 0) {
					cout << "\t>> ";
				}
				cout << "Exit" << endl;
				for (int i = 0; i < Player.nowTier(); i++) {
					if (action[2] == i + 1) {
						cout << "\t>> ";
					}
					cout << i + i << ". " << Player.getArmor(i).Name << " / Tier: " << Player.getArmor(i).Tier << " Defence: " << Player.getArmor(i).Multiplier << endl;
				}
				break;
			case 7:
				// Инвентарь: Зелья
				if (action[2] > 0 && action[2] < 3) {
					if (action[2] == 1) {
						action[1]--;
					}
					else {
						action[1]++;
					}
				}
				if (action[1] > floor((1 + Player.nowTier()) * 1.5)+1) {
					action[1] = 0;
				}
				else if (action[1] < 0) {
					action[1] = floor((1 + Player.nowTier()) * 1.5)+1;
				}
				if (action[1] == 0) {
					cout << "\t>> ";
				}
				cout << "Exit" << endl;
				for (int i = 0; i < floor((1 + Player.nowTier()) * 1.5); i++) {
					if (action[2] == i + 1) {
						cout << "\t>> ";
					}
					cout << i + i << ". " << Player.getPotion(i).Name << " / Effect: " << Player.getPotion(i).sEffect << endl;
				}
				break;
			case 8:
				// Инвентарь: Реликвии
				if (action[2] > 0 && action[2] < 3) {
					if (action[2] == 1) {
						action[1]--;
					}
					else {
						action[1]++;
					}
				}
				if (action[1] > 3) {
					action[1] = 0;
				}
				else if (action[1] < 0) {
					action[1] = 3;
				}
				cout << "EQP: " << Player.nowRelic().Name << " Strenght: " << Player.nowRelic().EffectStrenght << endl << endl;
				if (action[1] == 0) {
					cout << "\t>> ";
				}
				cout << "Exit" << endl;
				for (int i = 0; i < Player.nowTier(); i++) {
					if (action[2] == i + 1) {
						cout << "\t>> ";
					}
					cout << i + i << ". " << Player.nowRelic().Name << " Strenght: " << Player.nowRelic().EffectStrenght << endl;
				}
				break;
			case 9:
				// Магазин: Подменю (выбор тира)
				if (action[2] > 0 && action[2] < 3) {
					if (action[2] == 1) {
						action[1]--;
					}
					else {
						action[1]++;
					}
				}
				if (action[1] > 3) {
					action[1] = 0;
				}
				else if (action[1] < 0) {
					action[1] = 3;
				}
				cout << "Store / Select tier:" << endl;
				if (action[1] == 0) {
					cout << "\t>> ";
				}
				cout << "Back" << endl;
				if (action[1] == 1) {
					cout << "\t>> ";
				}
				cout << "Tier 1" << endl;
				if (action[1] == 2) {
					cout << "\t>> ";
				}
				cout << "Tier 2" << endl;
				if (action[1] == 3) {
					cout << "\t>> ";
				}
				cout << "Tier 3" << endl;
				break;
			case 10:
				// Магазин: Подменю (выбор типа)
				if (action[2] > 0 && action[2] < 3) {
					if (action[2] == 1) {
						action[1]--;
					}
					else {
						action[1]++;
					}
				}
				if (action[1] > 4) {
					action[1] = 0;
				}
				else if (action[1] < 0) {
					action[1] = 4;
				}
				cout << "Store / Select type:" << endl;
				if (action[1] == 0) {
					cout << "\t>> ";
				}
				cout << "Back" << endl;
				if (action[1] == 1) {
					cout << "\t>> ";
				}
				cout << "Weapon" << endl;
				if (action[1] == 2) {
					cout << "\t>> ";
				}
				cout << "Armor" << endl;
				if (action[1] == 3) {
					cout << "\t>> ";
				}
				cout << "Potions" << endl;
				if (action[1] == 4) {
					cout << "\t>> ";
				}
				cout << "Relics" << endl;
				break;
			case 11:
				// Магазин: Оружие
				if (action[2] > 0 && action[2] < 3) {
					if (action[2] == 1) {
						action[1]--;
					}
					else {
						action[1]++;
					}
				}
				if (action[1] > 6) {
					action[1] = 0;
				}
				else if (action[1] < 0) {
					action[1] = 6;
				}
				cout << "Your gems: " << Player.getGem() << endl;
				if (action[1] == 0) {
					cout << "\t>>";
				}
				cout << "Back" << endl;
				for (int i = 1 + (6 * (selectedTier - 1));i< 1 + (6 * (selectedTier));i++) {
					if (action[1] + (6 * (selectedTier - 1)) == i) {
						cout << "\t>>"; 
					}
					cout << Player.allWeapons[i].Name << "\tTier: " << Player.allWeapons[i].Tier << "\tPrice: " << Player.allWeapons[i].Price << endl;
				}
				break;
			case 12:
				// Магазин: Броня
				if (action[2] > 0 && action[2] < 3) {
					if (action[2] == 1) {
						action[1]--;
					}
					else {
						action[1]++;
					}
				}
				if (action[1] > 6) {
					action[1] = 0;
				}
				else if (action[1] < 0) {
					action[1] = 6;
				}
				cout << "Your gems: " << Player.getGem() << endl;
				if (action[1] == 0) {
					cout << "\t>>";
				}
				cout << "Back" << endl;
				for (int i = 1 + (1 * (selectedTier - 1)); i < 1 + (1 * (selectedTier)); i++) {
					if (action[1] + (1 * (selectedTier - 1)) == i) {
						cout << "\t>>";
					}
					cout << Player.allArmors[i].Name << "\tTier" << Player.allArmors[i].Tier << "\tPrice" << Player.allArmors[i].Price << endl;
				}
				break;
			case 13:
				// Магазин: Зелья
				if (action[2] > 0 && action[2] < 3) {
					if (action[2] == 1) {
						action[1]--;
					}
					else {
						action[1]++;
					}
				}
				if (action[1] > 6) {
					action[1] = 0;
				}
				else if (action[1] < 0) {
					action[1] = 6;
				}
				if (action[1] == 0) {
					cout << "\t>>";
				}
				cout << "Back" << endl;
				for (int i = 1 + (6 * (selectedTier - 1)); i < 1 + (6 * (selectedTier)); i++) {
					if (action[1] + (6 * (selectedTier - 1)) == i) {
						cout << "\t>>";
					}
					cout << Player.allWeapons[i].Name << "\tTier" << Player.allWeapons[i].Tier << "\tPrice" << Player.allWeapons[i].Price << endl;
				}
				break;
			case 14:
				// Магазин: Реликвии
				if (action[2] > 0 && action[2] < 3) {
					if (action[2] == 1) {
						action[1]--;
					}
					else {
						action[1]++;
					}
				}
				if (action[1] > 6) {
					action[1] = 0;
				}
				else if (action[1] < 0) {
					action[1] = 6;
				}
				if (action[1] == 0) {
					cout << "\t>>";
				}
				cout << "Back" << endl;
				for (int i = 1 + (6 * (selectedTier - 1)); i < 1 + (6 * (selectedTier)); i++) {
					if (action[1] + (6 * (selectedTier - 1)) == i) {
						cout << "\t>>";
					}
					cout << Player.allWeapons[i].Name << "\tTier" << Player.allWeapons[i].Tier << "\tPrice" << Player.allWeapons[i].Price << endl;
				}
				break;
			}
			update[0] = 0;
			action[2] = 0;
			action[3] = 0;
		}

	}
}

PlayerSystem preLoad(PlayerSystem Player) {
	//Предзагрузка оружия
	Weapon weapon;
	weapon.Name = "Fists";
	weapon.Tier = 0;
	weapon.Damage = 1;
	weapon.DamageType[0] = 1;
	weapon.DamageType[1] = 0;
	weapon.CritChance = 0;
	weapon.GemPerShot = 0;
	weapon.Price = 0;
	Player.allWeapons.push_back(weapon);
	weapon.Name = "Rusty Sword";
	weapon.Tier = 1;
	weapon.Damage = 3;
	weapon.DamageType[0] = 1;
	weapon.DamageType[1] = 0;
	weapon.CritChance = 3;
	weapon.GemPerShot = 0;
	weapon.Price = 10;
	Player.allWeapons.push_back(weapon);
	weapon.Name = "Cursed knife";
	weapon.Tier = 1;
	weapon.Damage = 1;
	weapon.DamageType[0] = 2;
	weapon.DamageType[1] = 0;
	weapon.CritChance = 5;
	weapon.GemPerShot = 10;
	weapon.Price = 10;
	Player.allWeapons.push_back(weapon);
	weapon.Name = "Hot stick";
	weapon.Tier = 1;
	weapon.Damage = 5;
	weapon.DamageType[0] = 3;
	weapon.DamageType[1] = 0;
	weapon.CritChance = 4;
	weapon.GemPerShot = 20;
	weapon.Price = 15;
	Player.allWeapons.push_back(weapon);
	weapon.Name = "Handmade bow";
	weapon.Tier = 1;
	weapon.Damage = 2;
	weapon.DamageType[0] = 1;
	weapon.DamageType[1] = 1;
	weapon.CritChance = 10;
	weapon.GemPerShot = 15;
	weapon.Price = 13;
	Player.allWeapons.push_back(weapon);
	weapon.Name = "Poisoned darts";
	weapon.Tier = 1;
	weapon.Damage = 2;
	weapon.DamageType[0] = 2;
	weapon.DamageType[1] = 1;
	weapon.CritChance = 5;
	weapon.GemPerShot = 10;
	weapon.Price = 10;
	Player.allWeapons.push_back(weapon);
	weapon.Name = "Dirty Spellbook";
	weapon.Tier = 1;
	weapon.Damage = 8;
	weapon.DamageType[0] = 3;
	weapon.DamageType[1] = 1;
	weapon.CritChance = 20;
	weapon.GemPerShot = 30;
	weapon.Price = 15;
	Player.allWeapons.push_back(weapon);
	weapon.Name = "Polished sword";
	weapon.Tier = 2;
	weapon.Damage = 10;
	weapon.DamageType[0] = 1;
	weapon.DamageType[1] = 0;
	weapon.CritChance = 3;
	weapon.GemPerShot = 0;
	weapon.Price = 20;
	Player.allWeapons.push_back(weapon);
	weapon.Name = "Poisoned knucles";
	weapon.Tier = 2;
	weapon.Damage = 5;
	weapon.DamageType[0] = 2;
	weapon.DamageType[1] = 0;
	weapon.CritChance = 5;
	weapon.GemPerShot = 10;
	weapon.Price = 20;
	Player.allWeapons.push_back(weapon);
	weapon.Name = "Magic spirit";
	weapon.Tier = 2;
	weapon.Damage = 15;
	weapon.DamageType[0] = 3;
	weapon.DamageType[1] = 0;
	weapon.CritChance = 4;
	weapon.GemPerShot = 20;
	weapon.Price = 30;
	Player.allWeapons.push_back(weapon);
	weapon.Name = "Crossbow";
	weapon.Tier = 2;
	weapon.Damage = 12;
	weapon.DamageType[0] = 1;
	weapon.DamageType[1] = 1;
	weapon.CritChance = 10;
	weapon.GemPerShot = 15;
	weapon.Price = 25;
	Player.allWeapons.push_back(weapon);
	weapon.Name = "Poisoned shuriken";
	weapon.Tier = 2;
	weapon.Damage = 5;
	weapon.DamageType[0] = 2;
	weapon.DamageType[1] = 1;
	weapon.CritChance = 5;
	weapon.GemPerShot = 10;
	weapon.Price = 20;
	Player.allWeapons.push_back(weapon);
	weapon.Name = "Explosion spellbook";
	weapon.Tier = 2;
	weapon.Damage = 20;
	weapon.DamageType[0] = 3;
	weapon.DamageType[1] = 1;
	weapon.CritChance = 20;
	weapon.GemPerShot = 30;
	weapon.Price = 35;
	Player.allWeapons.push_back(weapon);
	weapon.Name = "SuperSword";
	weapon.Tier = 3;
	weapon.Damage = 20;
	weapon.DamageType[0] = 1;
	weapon.DamageType[1] = 0;
	weapon.CritChance = 3;
	weapon.GemPerShot = 0;
	weapon.Price = 50;
	Player.allWeapons.push_back(weapon);
	weapon.Name = "Death knife";
	weapon.Tier = 3;
	weapon.Damage = 10;
	weapon.DamageType[0] = 2;
	weapon.DamageType[1] = 0;
	weapon.CritChance = 5;
	weapon.GemPerShot = 10;
	weapon.Price = 40;
	Player.allWeapons.push_back(weapon);
	weapon.Name = "Rod of Light";
	weapon.Tier = 3;
	weapon.Damage = 30;
	weapon.DamageType[0] = 3;
	weapon.DamageType[1] = 0;
	weapon.CritChance = 4;
	weapon.GemPerShot = 20;
	weapon.Price = 60;
	Player.allWeapons.push_back(weapon);
	weapon.Name = "Gun";
	weapon.Tier = 3;
	weapon.Damage = 25;
	weapon.DamageType[0] = 1;
	weapon.DamageType[1] = 1;
	weapon.CritChance = 10;
	weapon.GemPerShot = 15;
	weapon.Price = 45;
	Player.allWeapons.push_back(weapon);
	weapon.Name = "Cursed bow";
	weapon.Tier = 3;
	weapon.Damage = 20;
	weapon.DamageType[0] = 2;
	weapon.DamageType[1] = 1;
	weapon.CritChance = 5;
	weapon.GemPerShot = 10;
	weapon.Price = 35;
	Player.allWeapons.push_back(weapon);
	weapon.Name = "Spellbook of Ligtning";
	weapon.Tier = 3;
	weapon.Damage = 40;
	weapon.DamageType[0] = 3;
	weapon.DamageType[1] = 1;
	weapon.CritChance = 20;
	weapon.GemPerShot = 30;
	weapon.Price = 80;
	Player.allWeapons.push_back(weapon);
	Armor armor;
	armor.Name = "Old shabby leather jacket";
	armor.Tier = 1;
	armor.Multiplier = 1;
	armor.Price = 0;
	Player.allArmors.push_back(armor);
	armor.Name = "Leather jacket";
	armor.Tier = 1;
	armor.Multiplier = 0.95;
	armor.Price = 10;
	Player.allArmors.push_back(armor);
	armor.Name = "Leather jacket with chain inserts";
	armor.Tier = 2;
	armor.Multiplier = 0.85;
	armor.Price = 50;
	Player.allArmors.push_back(armor);
	armor.Name = "Energy-saturated fabric mantle with chain mail inserts";
	armor.Tier = 3;
	armor.Multiplier = 0.7;
	armor.Price = 135;
	Player.allArmors.push_back(armor);
	return Player;
}
int main()
{
	srand(time(0));
	if (!greeting()) { // НАЧАЛО ИГРЫ
		game();
		return 0;
	}
	else { // ВЫХОД ИЗ ИГРЫ
		return 0;
	}
	/*map.print(Player.getCoordinateX(), Player.getCoordinateY());
	while(true){
		bool update = false;
		if (GetKeyState(VK_LEFT) & 0x8000) {
			Player.move(-1,0);
			update = true;
		}
		if (GetKeyState(VK_RIGHT) & 0x8000) {
			Player.move(1,0);
			update = true;
		}
		if (GetKeyState(VK_DOWN) & 0x8000) {
			Player.move(0,1);
			update = true;
		}
		if (GetKeyState(VK_UP) & 0x8000) {
			Player.move(0,-1);
			update = true;
		}
		if (update) {
			map.print(Player.getCoordinateX(), Player.getCoordinateY());
			Sleep(500);
		}
	}*/
	//Player = battle(Player,0,3);
	return 0;
}